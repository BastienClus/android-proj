package com.example.bonjourmondedut2g5;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity3 extends AppCompatActivity {

    private String inputString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_3);

        inputString = getIntent().getExtras().getString("string_key");
    }

    public void onClickQuit3(View view) {
        Intent intentResult = new Intent();

        Bundle bundleResult = new Bundle();
        bundleResult.putString("string_key_send", inputString.toUpperCase());
        intentResult.putExtras(bundleResult);
        setResult(RESULT_OK , intentResult);

        finish();
    }


}
