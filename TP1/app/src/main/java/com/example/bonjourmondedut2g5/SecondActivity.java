package com.example.bonjourmondedut2g5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

    private int inputInteger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_second);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        inputInteger = bundle.getInt("integer_key");

        Log.d("bast", "log inputInteger : " + inputInteger);

    }

    public void onClickQuitSecondActivity(View view) {
        Intent intentResult = new Intent();

        Bundle bundleResult = new Bundle();
        bundleResult.putInt("integer_key_send", -inputInteger);
        intentResult.putExtras(bundleResult);
        setResult(RESULT_OK , intentResult);


        finish();
    }
}
