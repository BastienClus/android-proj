package com.example.bonjourmondedut2g5;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActiviteBonjourMonde extends AppCompatActivity {

    private int count;

    private final static int REQUEST_CODE_SECOND_ACTIVITY = 13;

    private final static int REQUEST_CODE_THIRD_ACTIVITY = 3;

    private TextView TextViewcount;

    private Button myButton;

    private Button boutonActivite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activite_bonjour_monde);

        count = 0;

        TextViewcount = findViewById(R.id.id_clickedtimes);

        myButton=findViewById(R.id.id_buttonplus);

        boutonActivite = findViewById(R.id.id_lance_activite);

        boutonActivite.setOnClickListener((v)->{

            gererLancerActivite();
        });
    }

    private void gererLancerActivite() {
        Intent intent = new Intent(this, SecondActivity.class);

        Bundle bundle = new Bundle();
        bundle.putInt("integer_key", count);
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
    }

    public void onClickPlusOne(View view) {
        count++;
        updateTextView("you clicked " + count +  " times");

    }

    private void updateTextView(String message){
        TextViewcount.setText(message);
    }

    public void onClickQuitPrincipal(View view) {
        finish();
    }

    public void onClickL3A(View view) {
        Intent intent = new Intent(this, Activity3.class);

        Bundle bundle = new Bundle();
        bundle.putString("string_key", "toto");
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST_CODE_THIRD_ACTIVITY);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_SECOND_ACTIVITY: {
                if(resultCode == RESULT_OK){
                    int resultat = data.getExtras().getInt("integer_key_send");

                    Toast.makeText(this, ""+resultat, Toast.LENGTH_SHORT).show();


                } else {

                }
            }
            ;
            break;

            case REQUEST_CODE_THIRD_ACTIVITY: {
                if(resultCode == RESULT_OK){
                    String result = data.getExtras().getString("string_key_send");

                    Toast.makeText(this, ""+result, Toast.LENGTH_SHORT).show();

                } else{

                }
            }
            ;
            break;
        }
    }
}
